import sys

def decode_message_recursive(header, encoded_message, key_to_char, key_length, i, decoded_message):
    while encoded_message[i : i+key_length] != "1" * key_length:
        key = encoded_message[i : i+key_length]
        decoded_message += key_to_char[key]
        i += key_length
    i += key_length
    key_length = int(encoded_message[i : i+3], 2)
    if key_length == 0:
        return decoded_message
    i += 3
    return decode_message_recursive(header, encoded_message, key_to_char, key_length, i, decoded_message)

def decode_message(header, encoded_message):
    key_to_char = {generate_keys(8)[i] : header[i] for i in range(len(header))}
    decoded_message = ""
    key_length = int(encoded_message[ : 3], 2)
    i = 3
    return decode_message_recursive(header, encoded_message, key_to_char, key_length, i, decoded_message)

def generate_keys(n):
    return [bin(j)[2 : ].zfill(i) for i in range(n) for j in range(2**i - 1) if bin(j)[2 : ] != '1' * i]

header = " ".join(i for i in sys.argv[1 : -1] if "0" not in i)
encoded_message = sys.argv[-1]
print(decode_message(header, encoded_message))
